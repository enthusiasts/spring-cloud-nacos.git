package com.lee.config;

import com.lee.constants.RabbitConstants;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

//配置队列信息
@Configuration
public class RabbitQueueConfig {

    //队列过期时间
    private int orderQueueTTL = 3000;

    // 配置普通队列
    @Bean
    public Queue orderQueue() {
        Map<String,Object> deadParamsMap = new HashMap<String, Object>();
        // 设置死信队列的Exchange
        deadParamsMap.put("x-dead-letter-exchange",RabbitConstants.DEAD_EXCHANGE);
        //设置死信队列的RouteKey
        deadParamsMap.put("x-dead-letter-routing-key",RabbitConstants.DEAD_ROUTE_KEY);
        // 设置对接过期时间"x-message-ttl"
//        deadParamsMap.put("x-message-ttl",orderQueueTTL);
        // 设置对接可以存储的最大消息数量
        deadParamsMap.put("x-max-length",20);
//        return QueueBuilder.durable(RabbitConstants.ORDER_QUEUE)
//                .withArguments(deadParamsMap)
//                .build();
        return new Queue(RabbitConstants.ORDER_QUEUE,true,false,false,deadParamsMap);
    }

    // 普通交换机
    @Bean
    public TopicExchange orderTopicExchange() {
        return new TopicExchange(RabbitConstants.ORDER_EXCHANGE);
    }

    // 绑定
    @Bean
    public Binding orderBinding() {
        return BindingBuilder.bind(orderQueue())
                .to(orderTopicExchange())
                .with(RabbitConstants.ORDER_ROUTE_KEY);
    }

    //配置死信队列
    @Bean
    public Queue deadQueue() {
//        return QueueBuilder.durable(RabbitConstants.DEAD_QUEUE)
////                .withArguments(deadParamsMap)
//                .build();
        return new Queue(RabbitConstants.DEAD_QUEUE);
    }

    // 死信交换机
    @Bean
    public DirectExchange deadExchange() {
        return new DirectExchange(RabbitConstants.DEAD_EXCHANGE);
    }

    // 死信  绑定
    @Bean
    public Binding deadBinding() {
        return BindingBuilder.bind(deadQueue())
                .to(deadExchange())
                .with(RabbitConstants.DEAD_ROUTE_KEY);

    }

}
