package com.lee.constants;

//定义个常量类
public class RabbitConstants {

    //订单队列
    public static final String ORDER_ROUTE_KEY = "order_route_key2";
    public static final String ORDER_EXCHANGE = "order_exchange2";
    public static final String ORDER_QUEUE = "order_queue_test2";

    //死信队列
    public static final String DEAD_QUEUE = "dead_queue2";
    public static final String DEAD_EXCHANGE = "dead_exchange2";
    public static final String DEAD_ROUTE_KEY = "dead_route_key2";
}

