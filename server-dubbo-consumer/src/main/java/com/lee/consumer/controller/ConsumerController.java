package com.lee.consumer.controller;

import com.lee.consumer.feign.ProviderFeign;
import com.lee.service.IDubboProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    @Autowired
    private ProviderFeign providerFeign ;
    @Value("${consumer.name}")
    private String consumerName ;
    @Value("${age}")
    private String age ;

    @org.apache.dubbo.config.annotation.Reference
    private IDubboProviderService iDubboProviderService ;

    /**
     * dubbo 远程调用
     * @param info
     * @return
     */
    @GetMapping("/getDubboRemoteInfo")
    public String getDubboRemoteInfo(@RequestParam("info") String info){

        return iDubboProviderService.getRemoteDubboInfo(info+"，消费者名称："+consumerName+"，年龄："+age);
    }

    /**
     * feign 远程调用
     * @param info
     * @return
     */
    @GetMapping("/getFeignRemoteInfo")
    public String getFeignRemoteInfo(@RequestParam("info") String info){

        return providerFeign.getRemoteFeignInfo(info+"，消费者名称："+consumerName);
    }
}
