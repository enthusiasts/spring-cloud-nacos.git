package com.lee.consumer.feign;

import com.lee.service.IProviderService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@Component
@FeignClient(value = "server-dubbo-provider")
public interface ProviderFeign extends IProviderService {
}
