package com.lee.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    @Autowired
    private LoadBalancerClient loadBalancerClient ;
    @Autowired
    private RestTemplate restTemplate ;

    @GetMapping("/getInfo")
    public String getInfo(@RequestParam("info") String info){

        // 通过服务提供者的服务id，获取对应的服务实例信息
        ServiceInstance serviceInstance =
                loadBalancerClient.choose("server-provider");

        // 远程接口服务
        String url= serviceInstance.getUri()+"/provider/getInfo?info="+info;

        String result = restTemplate.getForObject(url, String.class);

        System.out.println("远程调用结果");

        return result ;
    }

}
