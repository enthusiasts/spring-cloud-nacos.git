package com.lee.provider.service;

import com.lee.service.IDubboProviderService;

@org.apache.dubbo.config.annotation.Service
public class DubboProviderServiceImpl implements IDubboProviderService {
    public String getRemoteDubboInfo(String info) {
        return "Dubbo Provider Response ："+info;
    }
}
