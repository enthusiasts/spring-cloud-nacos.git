package com.lee.provider.service;

import com.lee.service.IProviderService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderServiceImpl implements IProviderService {

    public String getRemoteFeignInfo(String info) {
        return "Feign 远程调用，"+info;
    }
}
