package com.lee.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/provider")
public interface IProviderService {

    /**
     * 获取信息
     * @param info
     * @return
     */
    @GetMapping("/getRemoteFeignInfo")
    String getRemoteFeignInfo(@RequestParam(value = "info") String info);
}
