package com.lee.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/dubboProvider")
public interface IDubboProviderService {

    /**
     * 获取信息
     * @param info
     * @return
     */
    @GetMapping("/getRemoteDubboInfo")
    String getRemoteDubboInfo(@RequestParam(value = "info") String info);
}
